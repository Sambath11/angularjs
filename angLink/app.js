var myApp=angular.module('myApp',['ngRoute']);
myApp.config(function($routeProvider){
    $routeProvider
    .when('/',{
        templateUrl:'/first.html',
        controller:'firstController'
    }),
    $routeProvider
    .when('/second',{
            templateUrl:'/second.html',
            controller:'secondController'
        })
});
myApp.controller('firstController',['$scope','$log',function($scope,$log){
    $scope.name="First";
    
}]);
myApp.controller('secondController',['$scope','$log',function($scope,$log){
    $scope.name="Second";
}]);